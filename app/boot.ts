/// <reference path="../typings/index.d.ts" />
import * as angular from 'angular';
import 'angular-route';
import main from './main/main.module';
import news from './news/news.module';
import events from './events/events.module';
import { NavbarController }  from './navbar.controller';
import { SearchService }  from './search.service';

angular
    .module('app', [
        'ngRoute',
        main,
        news,
        events
    ]).controller('NavbarController', NavbarController)
    .service('SearchService', SearchService);

// Bootstrap the angular app module
angular
    .bootstrap(document.documentElement, ['app']);
