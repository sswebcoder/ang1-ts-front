/// <reference path="../../typings/index.d.ts" />
import * as angular  from 'angular';
import 'angular-route';
//import {PhoneListComponent} from './phone-list.component';

const moduleName = 'events'
export default moduleName;
angular.module(moduleName, ['ngRoute'])
    .config(['$routeProvider', ($routeProvider: angular.route.IRouteProvider) => { $routeProvider.when('/events', {
        templateUrl: './events/events.template.html',
        //controller: 'View1Ctrl'
      });
    }]);
    //.component('phoneList', new PhoneListComponent());
