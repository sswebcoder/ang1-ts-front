import * as ng  from 'angular';
import 'angular-route';

const moduleName = 'main'
export default moduleName;
ng.module(moduleName, ['ngRoute'])
    .config(['$routeProvider', ($routeProvider: ng.route.IRouteProvider) => { $routeProvider.
            when('/main', {
                templateUrl: './main/main.template.html',
                //controller: 'View1Ctrl'
              }).
            otherwise('/main');
    }]);
