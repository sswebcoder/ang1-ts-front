/// <reference path="../typings/index.d.ts" />
import * as ng from 'angular';
import { INews } from './news/news.factory';
import { ISearchService } from './search.service';

interface INScope extends ng.IScope {
    searchText: string;
    isActive(viewLocation: string): boolean;
    searchChange(): void;
}

export class NavbarController {
    $inject = ['$scope'];
    constructor($scope: INScope, $location: ng.ILocationService, News: INews, SearchService: ISearchService) {
        $scope.isActive = (viewLocation: string) => {
            return viewLocation === $location.path();
        };
        $scope.searchChange = () => {
            SearchService.search($scope.searchText);
        }
    }
}
