import * as ng from 'angular';
import { INewsResource } from './news.factory';

export class ModalWindowController {
    static $inject = ['$uibModalInstance', 'article', 'News'];
    private article: any;
    private $uibModalInstance: any;

    constructor($uibModalInstance: any,  article: any, News: INewsResource) {
        this.$uibModalInstance = $uibModalInstance;
        this.article = News.get({id: article.id});
    }

    close() {
        this.$uibModalInstance.dismiss();
    }
}
