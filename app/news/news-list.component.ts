import {NewsListController} from './news-list.controller';

export class NewsListComponent {
    templateUrl: string = './news/news-list.template.html';
    controller: any = ['News', '$uibModal', 'SearchService', '$location', NewsListController];
    constructor() {}
}
