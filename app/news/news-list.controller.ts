import * as ng from 'angular';
import { INews, INewsResource } from './news.factory';
import { ModalWindowController } from './modal-window.controller';
import { ISearchService } from '../search.service';

export class NewsListController {
    static $inject = ['$uibModal', 'News'];
    private start: number = 0;
    private limit: number = 2;
    public news: Array<INews> = [];
    public News: INewsResource;
    public $uibModal: any;
    public $location: ng.ILocationService;
    constructor(News: INewsResource, $uibModal: any,
                SearchService: ISearchService, $location: ng.ILocationService) {
        SearchService.addSubscriber(this.search, this);
        this.$location = $location;
        this.News = News;
        this.$uibModal = $uibModal;
        // К сожалению лучшего решения для отображения 3х новостей на
        // главное не придумал.
        if (this.$location.path() === '/main') {
            this.limit = 3;
        }
        this.news = News.query({
            start: this.start,
            limit: this.limit
        });
    }
    search(text: string): void {
        var self = this;
        this.start = 0;
        this.News.query({
            search: text
        }, (news: Array<INews>) => {
            self.news = news;
        });
    }
    openModal(article: INews): void {
        this.$uibModal.open({
            animation: true,
            size: 'lg',
            templateUrl: './news/news.modal.template.html',
            controller: ModalWindowController,
            controllerAs: '$ctrl',
            resolve: {
                article: function () {
                    return article;
                }
            }
        });
    }
    close(): void {
        console.log(arguments);
    }

    loadMore(): void {
        var self = this;
        this.start += this.limit;
        this.News.query({
            start: this.start,
            limit: this.limit
        }, (news: INews) => {
            self.news = self.news.concat(news);
        });
    }

    loadMoreState(): boolean {
        return this.$location.path() !== '/news' || this.news.length === 0;
    }
}
