/// <reference path="../../typings/index.d.ts" />
import * as ng from 'angular';
import 'angular-resource';

export interface INews extends ng.resource.IResource<INews>
{
    id: number;
    title: string;
    briefly: string;
    content: string;
    image: string;
    date: string;
}

export interface INewsResource extends ng.resource.IResourceClass<INews> { }

export class NewsFactory implements INewsResource {
    static $inject = ['$resource'];
    constructor(private $resource: ng.resource.IResourceService) {
        return $resource('/api/news/:id', { id: '@id' }, { });
    }
}
