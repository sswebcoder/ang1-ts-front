import * as angular  from 'angular';
import 'angular-route';
import 'angular-resource';
import 'angular-sanitize';
import 'angular-ui-bootstrap';
import { NewsListComponent } from './news-list.component';
import { NewsFactory } from './news.factory';
import { ModalWindowController } from './modal-window.controller';

const moduleName = 'news'
export default moduleName;
angular.module(moduleName, ['ngRoute', 'ngResource', 'ui.bootstrap', 'ngSanitize'])
    .config(['$routeProvider', ($routeProvider: angular.route.IRouteProvider) => { $routeProvider.when('/news', {
        templateUrl: './news/news.template.html',
      });
    }])
    .factory('News', ['$resource', NewsFactory])
    .controller('ModalWindowController', ['$uibModalInstance', 'article', 'News', ModalWindowController])
    .component('newsList', new NewsListComponent());
