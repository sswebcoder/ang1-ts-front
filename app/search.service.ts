/// <reference path="../typings/index.d.ts" />
import * as ng from 'angular';

export interface ISearchService {
    addSubscriber(subscriber: any, context: any): void;
    search(text: string): void;
}

export class SearchService implements ISearchService {
    private subscribers: Array<any> = [];
    private timeoutId: number;
    constructor() {}
    addSubscriber(subscriber: any, context: any): void {
        this.subscribers.push(ng.bind(context, subscriber));
    }
    search(text: string): void {
        var self = this;
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(() => {
            self.delivery(text);
        }, 800);
    }
    private delivery(text: string): void {
        this.subscribers.forEach((subscriber) => {
            subscriber(text);
        });
    }
}
