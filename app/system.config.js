System.config({
    baseURL: "/",
    defaultJSExtensions: true,
    transpiler: "typescript",
    paths: {
        "npm:*": "node_modules/*"
    },

    map: {
        "angular": "npm:angular/angular.js",
        "angular-route": "npm:angular-route/angular-route.js",
        "angular-resource": "npm:angular-resource/angular-resource.js",
        "angular-sanitize": "npm:angular-sanitize/angular-sanitize.js",
        "angular-ui-bootstrap": "npm:angular-ui-bootstrap/dist/ui-bootstrap-tpls.js",
        "typescript": "npm:typescript/lib/typescript.js"
    },
    meta: {
        'angular': {
            format: 'global',
            exports: 'angular'
        },
    }
});
