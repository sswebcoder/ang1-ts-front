var proxyMiddleware = require('http-proxy-middleware');

module.exports = {
    "logLevel": "silent",
    "server": {
        "baseDir": "app",
        "routes": {
            "/node_modules": "node_modules"
        },
        middleware: {
            1: proxyMiddleware('/api', {
                target: 'http://localhost:3002',
                changeOrigin: true   // for vhosted sites, changes host header to match to target's host
            })
        }
    }
};
